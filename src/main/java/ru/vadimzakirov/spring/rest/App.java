package ru.vadimzakirov.spring.rest;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vadimzakirov.spring.rest.configuration.MyConfig;
import ru.vadimzakirov.spring.rest.entity.Employee;

import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MyConfig.class);

        Communication communication = context.getBean("communication", Communication.class);
//        List<Employee> allEmployees = communication.getAllEmployees();
//        System.out.println(allEmployees);

//        Employee empById = communication.getEmployee(5);
//        System.out.println(empById);

//        Employee emp = new Employee("Sveta", "Sokolova", "LAW", 1200);
//        emp.setId(8);
//        communication.saveEmployee(emp);

       // communication.deleteEmployee(8);
    }
}
